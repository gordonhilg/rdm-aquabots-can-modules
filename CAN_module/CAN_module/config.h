/*******************************************************
** File    	: iodriver.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: All configuration to control the software
**
**
*******************************************************/


#ifndef CONFIG_H_
#define CONFIG_H_

//_____ D E F I N I T I O N S __________________________________________________

// -------------- BOOT LOADER DEFINITION
#define     BOOT_LOADER_SIZE            0x2000  // Size in bytes: 8KB
#define     MAX_FLASH_SIZE_TO_ERASE     ( FLASH_SIZE - ((uint32_t)(BOOT_LOADER_SIZE)) )

// -------------- PROCESSOR DEFINITION
#define     MANUF_ID            0x1E        // ATMEL
#define     FAMILY_CODE         0x81        // AT90CANxxx family

#define     XRAM_END            XRAMEND     // Defined in "iocanxx.h"
#define     RAM_END             RAMEND      // Defined in "iocanxx.h"
#define     E2_END              E2END       // Defined in "iocanxx.h"
#define     FLASH_END           FLASHEND    // Defined in bytes in "iocanxx.h"
#define     FLASH_SIZE          ((uint32_t)(FLASH_END)) + 1 // Size in bytes
#define     FLASH_PAGE_SIZE     256         // Size in bytes, constant for AT90CANxx devices

//-------------- MCU DEFINITION
#define  PRODUCT_NAME        0x95        // 32 Kbytes of Flash
#define  PRODUCT_REV         0x00        // Rev 0
#define  _BOOT_CONF_TYPE_    0           // lower than 64K bytes

//-------------- BOARD DEFINITION
#define AVR_AT90CAN32
#define BOARD_VOLT					//Use for the voltage board
//#define BOARD_GPS					//Use for the gps board
//#define BOARD_IMU					//Use for the imu board

// -------------- UART DEFINITIONS
#define USE_UART UART_0
#define USE_TIMER16 TIMER16_3

// -------------- MCU LIB CONFIGURATION
#define FOSC           16000		// 16 MHz External cristal
#define F_CPU          (FOSC*1000) // Need for AVR GCC

/* -------------- FUSES/LOCKBITS CONFIGURATION
Fuses can be configurated in the Device Programming window (CTRL+SHIFT+P)
The configuration for this project is as follows:

BODLEVEL	= DISABLED
TA0SEL		= [ ]
OCDEN		= [ ]
JTAGEN		= [X]
SPIEN		= [ ]
WDTON		= [ ]
EESAVE		= [X]
BOOTSZ		= 4096W_3000
BOOTRST		= [ ]
CKDIV8		= [ ]
CKOUT		= [ ]
SUT_CKSEL	= EXTXOSC_8MHZ_16MHZ_1KCK_65MS

EXTENDED	= 0xFF (valid)
HIGH		= 0xB1 (valid)
LOW			= 0xCF (valid)

LB			= NO_LOCK
BLB0		= NO_LOCK
BLB1		= NO_LOCK

LOCKBIT		= 0xFF (valid) */

// -------------- CANBUS CONFIG
#define CAN_BAUDRATE 1000
#define CAN_RECEIVE_BUFFER_SIZE 100

#endif /* CONFIG_H_ */
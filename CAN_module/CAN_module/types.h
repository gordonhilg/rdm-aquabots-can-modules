/*******************************************************
** File    	: CAN_module.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: All sensor types, enumerations and data structs that are used by the software
**
**
*******************************************************/	

#ifndef TYPES_H_
#define TYPES_H_


typedef enum SensorClass
{
	VOLTAGE,
	CURRENT,
	CURRENT_TOTAL,
	GPS,
	ACCELEROMETER,
	COMPASS,
	GYROSCOPE
}SensorClass;

typedef enum BatteryTypes
{
	UNKNOWN,
	LEAD_12V,
	LEAD_24V,
	LIPO_1C,
	LIPO_2C,
	LIPO_3C,
	LIPO_6C,
	LIION_1C,
	LIION_2C,
	LIION_3C,
	LIION_6C
}BatteryTypes;


typedef struct _sensorData{
	SensorClass sensorType;	//type of sensor that this struct holds information of
	uint8_t dataSize;		//size of the CAN message (max 8 byte)
	uint8_t *dataBuffer;	//pointer to CAN message array
	
	void *pData;			//void sensordata pointer for sensor specific data
	
}sensorData;


typedef struct _voltageData{
	BatteryTypes batteryType;
	uint16_t measurement;
}voltageData;

typedef struct _currentData{
	uint32_t measurement;
}currentData;

typedef struct _gpsData{
	//This struct does not use floating point integers, therefore the format used is the same format as provided by the GPS chip, but without points or commas.
	//For example longitude could be 1425678, this should be interpreted as 00142.5678
	uint32_t UTCtime;	//hhmmss.sss
	uint8_t dirNSEW;	//hemisphere indicator; 1st bit:(0 = N, 1 = S), second bit:(0 = E, 1 = W)
	uint32_t latitude;	//ddmm.mmmm
	uint32_t longitude;	//dddmm.mmmm
	uint16_t speed;		//kmph ex:(0020.0)
	uint16_t course;	//degrees ex:(359.9)
	uint8_t satUsed;	//total of sats in use (1 to 12)
	uint16_t HDOP;		//horizontal delution of precision (00.0 to 99.9)
	uint8_t GPSqual;	//GPS quality indicator:
						// 	0: position fix unavailable
						// 	1: valid position fix, SPS mode
						// 	2: valid position fix, differential GPS mode
						// 	3: GPS PPS Mode, fix valid
						// 	4: Real Time Kinematic. System used in RTK mode with fixed integers
						// 	5: Float RTK. Satellite system used in RTK mode. Floating integers
						// 	6: Estimated (dead reckoning) Mode
						// 	7: Manual Input Mode
						// 	8: Simulator Mode
	
}gpsData;

typedef struct _blinkTimerStruct{
	uint32_t HB;
	uint32_t Green;
	uint32_t Red;
	uint32_t Blue;
	uint32_t HBtimes;
	uint32_t Greentimes;
	uint32_t Redtimes;
	uint32_t Bluetimes;
	uint32_t HBmillis;
	uint32_t Greenmillis;
	uint32_t Redmillis;
	uint32_t Bluemillis;
}blinkTimerStruct;

typedef struct _canRcvData{
	uint8_t dataSize;		//size of the CAN message (max 8 byte)
	uint8_t dataBuffer[8];	//pointer to CAN message array
}canRcvData;

typedef struct _pulseTimerStruct{
	uint32_t HB;
	uint32_t Green;
	uint32_t Red;
	uint32_t Blue;
}pulseTimerStruct;

typedef struct _offTimerStruct{
	uint32_t HB;
	uint32_t Green;
	uint32_t Red;
	uint32_t Blue;
	uint8_t offFlag;
}offTimerStruct;

typedef enum HBState
{
	ON,
	OFF,
	BLINK,
	PULSE
}HBState;

typedef enum Leds
{
	HEARTBEAT,
	RED,
	GREEN,
	BLUE
}Leds;



#endif /* TYPES_H_ */
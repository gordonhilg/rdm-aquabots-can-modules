/*******************************************************
** File    	: cantest.h
** Author  	: Aquabots Team 2B
** Date		: 19-6-2015
**
**
**
*******************************************************/
#include <avr/io.h>
#include "types.h"
#include "drivers/timer/timerlib.h"
#include "drivers/can/can_prot.h"

#ifndef CANTEST_H_
#define CANTEST_H_

extern uint8_t rcvBufferSize;

/*
 *	@brief test the canbus system, sets this node to receiving mode
 */
void canReceiveTest(void);

/*
 *	@brief test the canbus system, sets this node to sending mode.
 *	@param state the state it needs to be
 */
void canSendTest(void);

#endif /* CANTEST_H_ */

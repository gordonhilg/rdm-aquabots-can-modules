/*******************************************************
** File    	: CAN_module.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: This is the main file from where the code is started.
**	Board specific code should be run from here and programmed in main.
**
*******************************************************/

//_____ I N C L U D E S ________________________________________________________
#include <avr/io.h>
#include <avr/iocanxx.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdbool.h>

#include "config.h"
#include "drivers/iodriver.h"
#include "drivers/leds.h"
#include "drivers/can/can_lib.h"
#include "drivers/adc/adc_drv.h"
#include "drivers/adc/adc_lib.h"
#include "types.h"
#include "drivers/can/can_prot.h"
#include "cantest.h"
#include "drivers/timer/timerlib.h"
#include "drivers/uart/uart_drv.h"
#include "drivers/uart/uart_lib.h"
#include "drivers/uart/uart_gps_prot.h"

//_____ D E C L A R A T I O N S ________________________________________________
void initIO();


//_____ D E F I N I T I O N S __________________________________________________
uint32_t runTime;		//time the MCU is running, in milliseconds
uint8_t ledISRFlag;		//flag for checking if the ISR was triggered


int main(void)
{
	//initialize all I/O pins
	initIO();
	initSysTick();
	
	//reset the CANbus and initialize it for sending data
	Can_reset();
	initializeCanBus(0);
	
	uart_init(CONF_8BIT_NOPAR_1STOP, 9600);
	gpsData rcvGPSData;
	
	while(1)//main loop to execute code in
	{
		if(uart_test_hit())
		{
			rdGPSData(&rcvGPSData);
		}		
	}	
	
	while(1);	
}

/*
 *	@brief This interrupt will trigger when there is a new CANbus message.
 *	It places this message in the receive buffer to handle at a later moment.
 */
ISR(CANIT_vect)
{	
	//check if a message has been received and handle it
	if((CANSTMOB & (1 << RXOK)) == (1 << RXOK))
	{
		//DONT FORGET TO FREE THIS OBJECT AFTER READING THE MESSAGE!!
		canRcvData *pMessage = (canRcvData *)malloc(sizeof(canRcvData));

		receiveCanData(pMessage);
	
		if(!placeDataInRcvBuffer(pMessage))
		{
			free(pMessage);
		}	
	}
	
	
	//handle errors if there are any
	if((CANGIT & 0x0F) != 0)
	{
		blinkLed(RED, 100, 10);
		CANGIT = 0b01111111;		//clear all pending interrupts on general register
	}
}

/*
 *	@brief This interrupt will trigger if the value in TIMER1 is the same as in OCR1A
 *	This way it will be triggered every 1  millisecond
 */
ISR(TIMER1_COMPA_vect)
{
	runTime++;
	ledISRFlag = 1;
}

/*
 *	@brief This function is used to configure all I/O registers 
 */
void initIO(void)
{
	//configure IO outputs
	
	//Set LED as output
	setOutput(PORTBnum, PB4);
	setOutput(PORTBnum, PB5);
	setOutput(PORTBnum, PB6);
	setOutput(PORTBnum, PB7);
	
	//set pins high
	setHigh(PORTBnum, PB4);
	setHigh(PORTBnum, PB5);
	setHigh(PORTBnum, PB6);
	setHigh(PORTBnum, PB7);
	
	//set heartbeat led to blink
	setHeartBeat(BLINK);
	
	setInput(PORTC, PC0);
	
	setInput(PORTAnum, PB0);
	setHigh(PORTAnum, PB0);
	setInput(PORTAnum, PB1);
	setHigh(PORTAnum, PB1);
	setInput(PORTAnum, PB2);
	setHigh(PORTAnum, PB2);
	setInput(PORTAnum, PB3);
	setHigh(PORTAnum, PB3);
}


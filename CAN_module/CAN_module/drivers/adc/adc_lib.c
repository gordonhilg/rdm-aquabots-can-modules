/*******************************************************
** File    	: adc_lib.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: this file contains functions to read the ADC and 
**	convert these values to the actual voltages connected to terminals
**
**
*******************************************************/

//_____  I N C L U D E S _______________________________________________________
#include "adc_lib.h"


uint32_t getVoltage(ADCPort port)
{
	if (port == VOLT12)
	{
		adc_init(AVCC_AS_VREF, NO_LEFT_ADJUST, ADC_CHANNEL_1);
		uint32_t adc = adc_single_conversion(ADC_CHANNEL_1);
		return (adc * 13.7773);
	}
	else if (port == VOLT24LEFT)
	{
		adc_init(AVCC_AS_VREF, NO_LEFT_ADJUST, ADC_CHANNEL_2);
		uint32_t adc = adc_single_conversion(ADC_CHANNEL_2);
		return (adc * 27.5844);
	}
	else if (port == VOLT24RIGHT)
	{
		adc_init(AVCC_AS_VREF, NO_LEFT_ADJUST, ADC_CHANNEL_3);
		uint32_t adc = adc_single_conversion(ADC_CHANNEL_3);
		return (adc * 27.5844);
	}
	else if (port == VOLT01)
	{
		adc_init(AVCC_AS_VREF, NO_LEFT_ADJUST, ADC_CHANNEL_0);
		uint32_t adc = adc_single_conversion(ADC_CHANNEL_0);
		adc = (adc * 0.947)-28;
		if(adc > 1024) adc = 0;
		return (adc); 
	}
	else
	{
		return 0;
	}	
}
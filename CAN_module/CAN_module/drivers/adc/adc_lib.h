/*******************************************************
** File    	: adc_lib.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: This file contains the functions to read the voltages from the screw terminals on the batteryboard
**
**
*******************************************************/


#ifndef ADC_LIB_H_
#define ADC_LIB_H_

//_____  I N C L U D E S _______________________________________________________

#include "../../config.h"
#include "adc_drv.h"
#include "adc_lib.h"

//_____ M A C R O S ____________________________________________________________


//_____ D E F I N I T I O N S __________________________________________________

//_____ D E C L A R A T I O N S ________________________________________________

typedef enum ADCPort
{
	VOLT12,
	VOLT24LEFT,
	VOLT24RIGHT,
	VOLT01
}ADCPort;


/*
 *	@brief Get the voltage of one of the screw terminals
 *	@param port The terminal you want to read
 */
uint32_t getVoltage(ADCPort port);





#endif /* ADC_LIB_H_ */
/*******************************************************
** File    	: leds.h
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
**
**
**
*******************************************************/


#ifndef RGB_H_
#define RGB_H_

#include <avr/io.h>
#include "iodriver.h"
#include "../types.h"


#define LED_PORT			PORTBnum
#define LED_PIN_HB			0x04
#define LED_PIN_RED			0x07
#define LED_PIN_GREEN		0x06
#define LED_PIN_BLUE		0x05

pulseTimerStruct pulseTimer;
blinkTimerStruct blinkTimer;
offTimerStruct offTimer;

extern uint32_t runTime;
extern uint8_t ledISRFlag;
/*
 *	@brief change the mode of the heartbeat led
 *	@param state the state it needs to be
 */
void setHeartBeat(HBState state);

/*
 *	@brief	pulse a led with ... milliseconds
 *	@param	ledType the ledtype to pulse
 *	@param	millis pulse this many milliseconds
 */
void pulseLed(Leds ledType, uint32_t millis);

/*
 *	@brief	blink a led with a given interval
 *	@param	ledType the ledtype to pulse
 *	@param	millis blink with this interval
 *	@param	times blink this many times, or enter 0 to blink forever
 */
void blinkLed(Leds ledtype, uint32_t millis, uint8_t times);

/*
 *	@brief ISR activated function to check if leds need to be shut off, this function should be called in the ISR
 */
void checkLEDStateISRPulse(void);

/*
 *	@brief ISR activated function to check if leds need to be toggled, this function should be called in the ISR
 */
void checkLEDStateISRBlink(void);

void checkLEDStateISROff(void);

void keepLEDOff(Leds ledtype, uint32_t millis);

void checkLEDS(void);

#endif /* RGB_H_ */

/*******************************************************
** File    	: timerlib.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: Timing related functions
**
**
*******************************************************/

#include "timerlib.h"


void initSysTick(void)
{
	// Enable CTC, prescaler at 64 bit (1 ms tick) (16 bit timer 1)
	TCCR1B = 0b00001011;
	// CTC at 250
	OCR1A = 250;
	// Clear pending interrupts for OCF1A
	TIFR1 = (1<<OCF1A);
	// Enable mask for compare function
	TIMSK1 = TIMSK1 | 0b10;
	// Enable global interrupts
	sei();
}

void waitFor(uint32_t millis)
{
	uint32_t startmillis = runTime;
	while(startmillis + millis > runTime);
	return;
}

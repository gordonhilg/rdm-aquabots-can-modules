/*******************************************************
** File    	: timerlib.h
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
**
**
**
*******************************************************/


#ifndef TIMERLIB_H_
#define TIMERLIB_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdbool.h>

extern uint32_t runTime;

/*
 *	@brief delay the system for ... milliseconds
 *	@param millis delay system for this many milliseconds
 */
void waitFor(uint32_t millis);

/*
 *	@brief init the timer for use with system tick
 */
void initSysTick(void);

#endif /* TIMERLIB_H_ */

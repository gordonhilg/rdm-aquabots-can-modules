/*******************************************************
** File    	: iodriver.h
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
**
**
**
*******************************************************/


#ifndef IODRIVER_H_
#define IODRIVER_H_

#include <avr/io.h>

#include "../config.h"

#define PORTAnum	0x02
#define PORTBnum	0x05
#define PORTCnum	0x08
#define PORTDnum	0x0B
#define PORTEnum	0x0E
#define PORTFnum	0x11
#define PORTGnum	0x14

#define BTN_PORT_IN		PORTCnum
#define BTN_PIN			PC0

//TODO: document these functions
void setOutput(uint8_t port, uint8_t pin);

void setInput(uint8_t port, uint8_t pin);

void setHigh(uint8_t port, uint8_t pin);

void setLow(uint8_t port, uint8_t pin);

uint8_t readPin(uint8_t port, uint8_t pin);

uint8_t readDipSwitches(void);

void togglePin(uint8_t port, uint8_t pin);

#endif /* IO_H_ */
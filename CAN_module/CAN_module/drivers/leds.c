/*******************************************************
** File    	: leds.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: This file contains functions to control the LEDs on the modules
**
**
*******************************************************/
#include "leds.h"

void checkLEDStateISRPulse(void)
{
	if(pulseTimer.HB == runTime)
	{
		setHigh(PORTBnum, LED_PIN_HB);
	}
	if(pulseTimer.Red == runTime)
	{
		setHigh(PORTBnum, LED_PIN_RED);
	}
	if(pulseTimer.Green == runTime)
	{
		setHigh(PORTBnum, LED_PIN_GREEN);
	}
	if(pulseTimer.Blue == runTime)
	{
		setHigh(PORTBnum, LED_PIN_BLUE);
	}
}

void checkLEDStateISROff(void)
{
	if(offTimer.HB == runTime)
	{
		offTimer.offFlag &= ~(1 << 0);
	}
	if(offTimer.Red == runTime)
	{
		offTimer.offFlag &= ~(1 << 1);
	}
	if(offTimer.Green == runTime)
	{
		offTimer.offFlag &= ~(1 << 2);
	}
	if(offTimer.Blue == runTime)
	{
		offTimer.offFlag &= ~(1 << 3);
	}
	
}

void keepLEDOff(Leds ledtype, uint32_t millis)
{
	switch(ledtype)
	{
		case HEARTBEAT:
		offTimer.offFlag |= 1 << 0;
		offTimer.HB = runTime + millis;
		break;
		case RED:
		offTimer.offFlag |= 1 << 1;
		offTimer.Red = runTime + millis;
		break;
		case GREEN:
		offTimer.offFlag |= 1 << 2;
		offTimer.Green = runTime + millis;
		break;
		case BLUE:
		offTimer.offFlag |= 1 << 3;
		offTimer.Blue = runTime + millis;
		break;
	}
	
}

void checkLEDStateISRBlink(void)
{
	if(blinkTimer.HB == runTime && blinkTimer.HBtimes == 0)
	{
		blinkTimer.HB = runTime + blinkTimer.HBmillis;
		togglePin(PORTBnum, LED_PIN_HB);	
	}	
	else if(blinkTimer.HB == runTime && blinkTimer.HBtimes != 1)
	{
		blinkTimer.HBtimes--;	
		blinkTimer.HB = runTime + blinkTimer.HBmillis;
		togglePin(PORTBnum, LED_PIN_HB);
	}
	
	if(blinkTimer.Red == runTime && blinkTimer.Redtimes == 0)
	{
		blinkTimer.Red = runTime + blinkTimer.Redmillis;
		togglePin(PORTBnum, LED_PIN_RED);
	}
	else if(blinkTimer.Red == runTime && blinkTimer.Redtimes != 1)
	{
		blinkTimer.Redtimes--;
		blinkTimer.Red = runTime + blinkTimer.Redmillis;
		togglePin(PORTBnum, LED_PIN_RED);
	}
	
	if(blinkTimer.Green == runTime && blinkTimer.Greentimes == 0)
	{
		blinkTimer.Green = runTime + blinkTimer.Greenmillis;
		togglePin(PORTBnum, LED_PIN_GREEN);
	}
	else if(blinkTimer.Green == runTime && blinkTimer.Greentimes != 1)
	{
		blinkTimer.Greentimes--;	
		blinkTimer.Green = runTime + blinkTimer.Greenmillis;	
		togglePin(PORTBnum, LED_PIN_GREEN);
	}
	
	if(blinkTimer.Blue == runTime && blinkTimer.Bluetimes == 0)
	{
		blinkTimer.Blue = runTime + blinkTimer.Bluemillis;
		togglePin(PORTBnum, LED_PIN_BLUE);
	}
	else if(blinkTimer.Blue == runTime && blinkTimer.Bluetimes != 1)
	{		
		blinkTimer.Bluetimes--;	
		blinkTimer.Blue = runTime + blinkTimer.Bluemillis;	
		togglePin(PORTBnum, LED_PIN_BLUE);
	}	
}

void blinkLed(Leds ledType, uint32_t millis, uint8_t times)
{
	switch(ledType)
	{
		case HEARTBEAT:
			if(blinkTimer.HBtimes < 2 && !((offTimer.offFlag >> 0) & 1))
			{		
				setLow(PORTBnum, LED_PIN_HB);
				blinkTimer.HB = runTime + millis;
				blinkTimer.HBtimes = (times * 2);
				blinkTimer.HBmillis = millis;
			}
			break;
		case RED:
			if(blinkTimer.Redtimes < 2 && !((offTimer.offFlag >> 1) & 1))
			{		
				setLow(PORTBnum, LED_PIN_RED);
				blinkTimer.Red = runTime + millis;
				blinkTimer.Redtimes = (times * 2);
				blinkTimer.Redmillis = millis;
			}
			break;
		case GREEN:
			if(blinkTimer.Greentimes < 2 && !((offTimer.offFlag >> 2) & 1))
			{
				setLow(PORTBnum, LED_PIN_GREEN);
				blinkTimer.Green = runTime + millis;
				blinkTimer.Greentimes = (times * 2);
				blinkTimer.Greenmillis = millis;
			}
			break;
		case BLUE:
			if(blinkTimer.Bluetimes < 2 && !((offTimer.offFlag >> 3) & 1))
			{
				setLow(PORTBnum, LED_PIN_BLUE);
				blinkTimer.Blue = runTime + millis;
				blinkTimer.Bluetimes = (times * 2);
				blinkTimer.Bluemillis = millis;
			}
			break;
	}
}

void pulseLed(Leds ledType, uint32_t millis)
{
	switch(ledType)
	{
		case HEARTBEAT:
			if(!((offTimer.offFlag >> 0) & 1)) 
			{
				setLow(PORTBnum, LED_PIN_HB);
				pulseTimer.HB = runTime + millis;
			}
			break;
		case RED:
			if(!((offTimer.offFlag >> 1) & 1)) 
			{
				setLow(PORTBnum, LED_PIN_RED);
				pulseTimer.Red = runTime + millis;
			}
			break;
		case GREEN:
			if(!((offTimer.offFlag >> 2) & 1))
			{ 
				setLow(PORTBnum, LED_PIN_GREEN);
				pulseTimer.Green = runTime + millis;
			}
			break;
		case BLUE:
			if(!((offTimer.offFlag >> 3) & 1)) 
			{
				setLow(PORTBnum, LED_PIN_BLUE);	
				pulseTimer.Blue = runTime + millis;
			}
			break;
	} 	
}

void setHeartBeat(HBState state)
{
	if (state == OFF)
	{
		setHigh(PORTBnum, PB4);
	}
	else if(state == ON)
	{
		setLow(PORTBnum, PB4);
	}
	else if(state == BLINK)
	{
		blinkLed(HEARTBEAT, 500, 0);
	}
	else if(state == PULSE)
	{
		//TODO: implement
	}
}

void checkLEDS(void)
{
	if(ledISRFlag == 1)
	{//led ISR check for blinking and stuff
		ledISRFlag = 0;
		checkLEDStateISRPulse();
		checkLEDStateISRBlink();
		checkLEDStateISROff();
	}
	
}


/*******************************************************
** File    	: iodriver.c
** Author  	: Aquabots Team 2B
** Date		: 29-5-2015
** Brief	: Functions to control I/O pins and read the dipswitches
**
**
*******************************************************/

#include "iodriver.h"

void setOutput(uint8_t port, uint8_t pin)
{
	_SFR_IO8(port-1) |= (1 << pin);
}

void setInput(uint8_t port, uint8_t pin)
{
	_SFR_IO8(port-1) &= ~(1<<pin);
}

void setHigh(uint8_t port, uint8_t pin)
{
	_SFR_IO8(port) |= (1 << pin);
}

void setLow(uint8_t port, uint8_t pin)
{
	_SFR_IO8(port) &= ~(1 << pin);
}

void togglePin(uint8_t port, uint8_t pin)
{
	_SFR_IO8(port) ^= (1 << pin);
}

uint8_t readPin(uint8_t port, uint8_t pin)
{
	uint8_t portPin = _SFR_IO8(port-2);


	if(portPin & (0x01 << pin) )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t readDipSwitches(void)
{
#ifdef BOARD_VOLT
	uint8_t pinMask = 0x0F;
	
	return pinMask & PINA;
#endif

#ifdef BOARD_GPS
	uint8_t portPin = PINC;
	uint8_t pinMask = 0x0F;
	return ((portPin >> 1) & pinMask);
#endif
	
	return 0;
}

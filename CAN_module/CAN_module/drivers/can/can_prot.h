/*
 * can_prot.h
 *
 * Created: 11-6-2015 11:05:25
 *  Author: Gordon
 */ 


#ifndef CAN_PROT_H_
#define CAN_PROT_H_

//_____ I N C L U D E S ________________________________________________________
#include <avr/io.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "../../config.h"
#include "can_lib.h"
#include "../../types.h"
#include "../iodriver.h"
#include "../leds.h"
#include "../timer/timerlib.h"
//_____ D E F I N I T I O N S __________________________________________________


//______DEFINITIONS OF ALL CAN MESSAGE TYPES
#define V_MES_SEND				0x01
#define V_MES_REQ				0x02

#define C_MES_SEND				0x03
#define C_MES_REQ				0x04

#define WH_TOTAL_SEND			0x05
#define WH_TOTAL_REQ			0x06

#define GPS_STATUS_SEND			0x07
#define GPS_STATUS_REQ			0x08
#define GPS_LAT					0x09
#define GPS_LONG				0x0A
#define GPS_FIX_INFO			0x0B

#define SYS_BOARD_SEND			0xC9
#define SYS_BOARD_REQ			0xCA
#define SYS_VERSION_SEND		0xCB
#define SYS_VERSION_REQ			0xCC

#define EXTENDED_MESSAGE		0xFF			




//_____ D E C L A R A T I O N S ________________________________________________
/*
 *	@brief Initialize the CANbus before sending and receiving data
 *	@param canID the ID this device should publish when sending data over CAN, use 0 to read DIP switches for the adress
 *  @return 1 if initialization is successfull
 */
uint8_t initializeCanBus(uint8_t canID);

/*
 *	@brief Get one item from the CAN data buffer
 *  @return 0 if the buffer is empty
 */
canRcvData * getRcvBufferData();

/*
 *	@brief place a CAN message in the buffer, this function should be used by the CAN ISR
 *	@param the message struct with the length and message of it in it
 *  @return 0 if the buffer is full
 */
uint8_t placeDataInRcvBuffer(canRcvData *newData);

/*	@brief create the CAN data for a voltage message
 *	@param sensor The sensor data
 *  @return Returns 1 if data is created correctly, 0 if not
 */
uint8_t createVoltageData(sensorData *sensor);

/*
 *	@brief create the CAN data for a GPS message
 *	@param sensor The sensor data
 *  @return Returns 1 if data is created correctly, 0 if not
 */
uint8_t createGPSData(sensorData *sensor, uint8_t dataType);


/*
 *	@brief create the CAN data for a current message
 *	@param sensor The sensor data struct
 *  @return Returns 1 if data is created correctly, 0 if not
 */
uint8_t createCurrentData(sensorData *sensor);

/*
 *	@brief send the prepared data over CAN
 *	@param sensor the sensor data
 */
void sendSensorData(sensorData *sensor);

/*
 *	@brief SUGGESTED NOT TO USE THIS FUNCTION! receive incoming data from CAN network. NOTE: This function does not use interrupts or a data buffer
 *	@param void
 *  @return Returns the CAN message status
 */
void receiveCanData(canRcvData *pMessage);

/* 
 *  @brief Process a received CAN bus message.
 *	This function should be used if the board specific code couldn't process the message.
 *  All non board specific messages should be handled in here to minimize double code.
 *	@param pMessage The message that should be processed
 */
void processMessage(canRcvData *pMessage);

/* 
 *  @brief send the type of this board via CANbus
 */
void sendBoardType(void);

#endif /* CAN_PROT_H_ */

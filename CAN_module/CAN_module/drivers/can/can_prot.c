/*******************************************************
** File    	: CAN_module.c
** Author  	: Aquabots Team 2B
** Date		: 11-6-2015
** Brief	: The functions in this file are used to send and receive data via CANbus and 
**	to create messages as described in the CAN protocol.
**
*******************************************************/



//_____ I N C L U D E S ________________________________________________________

#include "can_prot.h"

//_____ D E F I N I T I O N S __________________________________________________
static uint16_t canID;
volatile uint8_t rcvBufferSize;	//used items in the receive buffer
canRcvData *rcvBuffer[CAN_RECEIVE_BUFFER_SIZE];

st_cmd_t dataMsg;
uint8_t responseBuffer[8];
//_____ F U N C T I O N S ______________________________________________________


uint8_t initializeCanBus(uint8_t canAddress)
{
	if (canAddress == 0)
	{	//read address from DIP switches
		canID = readDipSwitches();
	} 
	else
	{	//use defines address
		canID = canAddress;
	}
	can_init(0);
	
	rcvBufferSize = 0;
	
	//initialize mOB for RX messages
	dataMsg.pt_data = &responseBuffer[0];
	for(uint8_t i=0; i<8; i++) responseBuffer[i]=0;
	dataMsg.cmd = CMD_RX_DATA;
	//--- Rx Command
	while(can_cmd(&dataMsg) != CAN_CMD_ACCEPTED);
	
	
	//Initialize CANbus interrupts
	CANGIE = 0b10100010;	// enable interrupt on receive and general error
	CANIE1 = 0xFF;		//enable interrupts on all message objects
	CANIE2 = 0xFF;
	
	CANGIT = 0b01111111;		//clear all pending interrupts on general register
	Can_clear_status_mob();		//clear all pending interrupts on mOB register
	
	return 1;
}

canRcvData * getRcvBufferData()
{
	if(rcvBufferSize == 0)
	{
		return 0;		//buffer is empty
	}
	canRcvData *rtnVal = rcvBuffer[0];
	
	//move all buffer items one place down
	for(uint8_t i = 1; i < rcvBufferSize; i++)
	{
		rcvBuffer[i-1] = rcvBuffer[i];
	}
	rcvBufferSize--;
	return rtnVal;
}


uint8_t placeDataInRcvBuffer(canRcvData *newData)
{
	
	if(rcvBufferSize >= CAN_RECEIVE_BUFFER_SIZE-1)
	{
		return 0;
	}
	rcvBuffer[rcvBufferSize] = newData;
	
	rcvBufferSize++;
	return 1;
}

uint8_t createVoltageData(sensorData *sensor)
{
	sensor->dataSize = 6;
	uint8_t *dataArray = malloc(sensor->dataSize * sizeof(uint8_t));
	
	if(dataArray == NULL)
	{
		return 0;
	}
	
	dataArray[0] = V_MES_SEND;	//first byte contains the message type
	
	switch(((voltageData*)sensor->pData)->batteryType)
	{
		case UNKNOWN:
			dataArray[1] = 0x00;
		break;
		case LEAD_12V:
			dataArray[1] = 0x01;
		break;
		case LEAD_24V:
			dataArray[1] = 0x02;
		break;
		case LIPO_1C:
			dataArray[1] = 0x03;
		break;
		case LIPO_2C:
			dataArray[1] = 0x04;
		break;
		case LIPO_3C:
			dataArray[1] = 0x05;
		break;
		case LIPO_6C:
			dataArray[1] = 0x06;
		break;
		case LIION_1C:
			dataArray[1] = 0x07;
		break;
		case LIION_2C:
			dataArray[1] = 0x08;
		break;
		case LIION_3C:
			dataArray[1] = 0x09;
		break;
		case LIION_6C:
			dataArray[1] = 0x0A;
		break;
		default:
			dataArray[1] = 0x00;
		break;
	}
	dataArray[1] |= 0x1 << 4;	//accu status
	dataArray[1] |= 0x1 << 7;	//delen
	uint16_t devideFactor = 1000;
	dataArray[2] = devideFactor;
	dataArray[3] = devideFactor >> 8;
	
	dataArray[4] = ((voltageData*)sensor->pData)->measurement;
	dataArray[5] = ((voltageData*)sensor->pData)->measurement >> 8;
	
	sensor->dataBuffer = dataArray;
	
	return 1;
}

uint8_t createGPSData(sensorData *sensor, uint8_t dataType)
{
	uint8_t rtnVal = 0;
	sensor->dataBuffer[0] = dataType;
	
	switch (dataType)
	{
		case GPS_STATUS_SEND:
		{
			sensor->dataSize = 8;
			sensor->dataBuffer[1] = ((gpsData*)sensor->pData)->satUsed << 4 | ((gpsData*)sensor->pData)->GPSqual;
			sensor->dataBuffer[2] = ((gpsData*)sensor->pData)->UTCtime;
			sensor->dataBuffer[3] = ((gpsData*)sensor->pData)->UTCtime >> 8;
			sensor->dataBuffer[4] = ((gpsData*)sensor->pData)->UTCtime >> 16;
			sensor->dataBuffer[5] = ((gpsData*)sensor->pData)->UTCtime >> 24;
			sensor->dataBuffer[6] = ((gpsData*)sensor->pData)->HDOP;
			sensor->dataBuffer[7] = ((gpsData*)sensor->pData)->HDOP >> 8;
			rtnVal = 1;
		}
		break;
		case GPS_LONG:
		{
			sensor->dataSize = 5;
			sensor->dataBuffer[1] = ((gpsData*)sensor->pData)->latitude;
			sensor->dataBuffer[2] = ((gpsData*)sensor->pData)->latitude >> 8;
			sensor->dataBuffer[3] = ((gpsData*)sensor->pData)->latitude >> 16;
			sensor->dataBuffer[4] = ((gpsData*)sensor->pData)->latitude >> 24;
			rtnVal = 1;
		}
		break;
		case GPS_LAT:
		{
			uint32_t tempUTCtime = ((gpsData*)sensor->pData)->UTCtime;
			tempUTCtime /= 1000; //remove the .sss part for sending
			sensor->dataSize = 8;
			sensor->dataBuffer[1] = ((gpsData*)sensor->pData)->longitude;	
			sensor->dataBuffer[2] = ((gpsData*)sensor->pData)->longitude >> 8;	
			sensor->dataBuffer[3] = ((gpsData*)sensor->pData)->longitude >> 16;	
			sensor->dataBuffer[4] = ((gpsData*)sensor->pData)->longitude >> 24;	
			sensor->dataBuffer[5] = tempUTCtime;
			sensor->dataBuffer[6] = tempUTCtime >> 8;
			sensor->dataBuffer[7] = tempUTCtime >> 16;
			rtnVal = 1;
		}
		break;
		case GPS_FIX_INFO:
		{
			sensor->dataSize = 6;
			sensor->dataBuffer[1] = ((gpsData*)sensor->pData)->dirNSEW << 4 | ((gpsData*)sensor->pData)->GPSqual;
			sensor->dataBuffer[2] = ((gpsData*)sensor->pData)->speed;
			sensor->dataBuffer[3] = ((gpsData*)sensor->pData)->speed >> 8;	
			sensor->dataBuffer[4] = ((gpsData*)sensor->pData)->course;	
			sensor->dataBuffer[5] = ((gpsData*)sensor->pData)->course >> 8;
			rtnVal = 1;	
		}
		break;
		default:
			break;	
	}		
	
	return rtnVal;
}

uint8_t createCurrentData(sensorData *sensor)
{
	sensor->dataSize = 5;
	uint8_t *dataArray = malloc(sensor->dataSize * sizeof(uint8_t));
	
	if(dataArray == NULL)
	{
		return 0;
	}
	
	dataArray[0] = C_MES_SEND;	//first byte contains the message type
	
	dataArray[1] = ((currentData*)sensor->pData)->measurement;
	dataArray[2] = ((currentData*)sensor->pData)->measurement >> 8;
	dataArray[3] = ((currentData*)sensor->pData)->measurement >> 16;
	dataArray[4] = ((currentData*)sensor->pData)->measurement >> 24;
	sensor->dataBuffer = dataArray;
	
	return 1;
}
	
void sendSensorData(sensorData *sensor)
{
	st_cmd_t message;
	message.ctrl.ide = 0;            //- CAN 2.0A
	message.dlc = sensor->dataSize;    //- Message with 2-byte data
	message.id.std = canID;
	message.pt_data = sensor->dataBuffer; 
	message.cmd = CMD_TX_DATA;	
	
	while(can_cmd(&message) != CAN_CMD_ACCEPTED);
	uint8_t canStatus;
	do
	{
		canStatus = can_get_status(&message);
	}
	while(canStatus == CAN_STATUS_NOT_COMPLETED);
	
	if(canStatus == CAN_STATUS_ERROR)
	{
		blinkLed(RED, 100, 10);
	}
}

void receiveCanData(canRcvData *pMessage)
{
	uint8_t canStatus;
	int i;
		
	for(i=0; i<8; i++) dataMsg.pt_data[i]=0;
	
	dataMsg.cmd = CMD_RX_DATA;
		
	// --- Wait for Rx completed
	while(1)
	{
		canStatus = can_get_status(&dataMsg);
		if (canStatus != CAN_STATUS_NOT_COMPLETED) break; // Out of while
	}
	
	//TODO: handle CAN status errors correctly
	if (canStatus == CAN_STATUS_ERROR);
	
	//copy CAN message size to pointer
	pMessage->dataSize = dataMsg.dlc;
	
	//copy CAN message to the pointer
	for(uint8_t i = 0; i < pMessage->dataSize;i++)
	{
		pMessage->dataBuffer[i] = dataMsg.pt_data[i];
	}

	//TODO: make sure we don't stay in this loop forever if there is a problem
	// --- Rx Command
	while(can_cmd(&dataMsg) != CAN_CMD_ACCEPTED);
}

void processMessage(canRcvData *pMessage)
{	//TODO: not tested
	//filter all SEND messages, we only want to handle request messages
	if((pMessage->dataBuffer[0] & 0x01) != 0x01)
	{
		switch(pMessage->dataBuffer[0])
		{
			case SYS_BOARD_REQ:
			{
				//Construct the request ID
				uint16_t reqCanID;
				reqCanID = pMessage->dataBuffer[1] | (pMessage->dataBuffer[2] << 8);
				if(reqCanID == canID || reqCanID == 0)
				{
					sendBoardType();
				}
			}
			break;

			default:
				//this is a not implemented message, so we can just ignore it.
			break;
		}
	}
}

void sendBoardType(void)
{	//TODO: not tested
	sensorData BoardTypeMessage;	//is a malloc necessary? 
									//data is only used inside of this function and then send from this function
	uint8_t sendData[4];
	BoardTypeMessage.dataBuffer = &sendData[0];
	BoardTypeMessage.dataSize = 4;
	sendData[0] = SYS_BOARD_SEND;
	sendData[1] = canID;
	sendData[2] = canID >> 8;
	#ifdef BOARD_VOLT
		sendData[3] = 0x02;
	#elif BOARD_GPS
		sendData[3] = 0x01;
	#elif BOARD_IMU
		sendData[3] = 0x03;
	#else
		sendData[3] = 0x00;
	#endif
		
	sendSensorData(&BoardTypeMessage);
}
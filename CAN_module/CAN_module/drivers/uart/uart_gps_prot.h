/*******************************************************
** File    	: uart_gps_prot.h
** Author  	: Aquabots Team 2B
** Date		: 7-7-2015
**
**
**
*******************************************************/


#ifndef UART_GPS_PROT_H_
#define UART_GPS_PROT_H_

//_____ I N C L U D E S ________________________________________________________
#include <string.h>
#include <stdlib.h>

#include "../uart/uart_drv.h"
#include "../uart/uart_lib.h"
#include "../../types.h"
#include "../leds.h"


//_____ D E C L A R A T I O N S ________________________________________________
/*
 *	@brief receive GPS data over uart and put the data in a struct
 *	@param rcvGPSData the struct where the GPS data is stored
 *  @return Returns 1 if data is created correctly, 0 if no data has been received
 */
uint8_t rdGPSData(gpsData *rcvGPSData);

/*
 *	@brief remove character(s) from a string
 *	@param s string to operate on
 *  @param toremove character(s) to remove
 */
void removeSubstring(char *s,const char *toremove);



#endif /* UART_GPS_PROT_H_ */
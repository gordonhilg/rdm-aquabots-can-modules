/*******************************************************
** File    	: uart_gps_prot.c
** Author  	: Aquabots Team 2B
** Date		: 7-7-2015
**
**
**
*******************************************************/

#include "uart_gps_prot.h"

uint8_t rdGPSData(gpsData *rcvGPSData)
{
	//Chars are being sent from the GPS chip to the microcontroller through UART. The data is being sent as ascii.
	char rcvChar;
	
	//Buffer used to temporarily store the incoming data before processing
	char NMEABuf[90] = "";
	
	uint8_t rtnVal = 0;
	uint8_t i = 0;
	rcvChar = uart_getchar();
	if(rcvChar == '$') // check for $, this is the beginning of a new message
	{
		while(rcvChar != 0x0a) // check for end of data transmit
		{
			rcvChar = uart_getchar();
			NMEABuf[i] = rcvChar;
			i++;
		}
	}

	if(!strncmp(NMEABuf,"GPGGA",5))
	{
		removeSubstring(NMEABuf, ".");
		strtok(NMEABuf, ",");
		rcvGPSData->UTCtime = atol(strtok(NULL, ","));
		rcvGPSData->latitude = atol(strtok(NULL, ","));
		if(*strtok(NULL, ",") == 78)
		{
			rcvGPSData->dirNSEW &= 0;
		}
		else
		{
			rcvGPSData->dirNSEW |= 1;
		}
		rcvGPSData->longitude = atol(strtok(NULL, ","));
		if(*strtok(NULL, ",") == 69)
		{
			rcvGPSData->dirNSEW &= ~(1 << 1);
		}
		else
		{
			rcvGPSData->dirNSEW |= (1 << 1);
		}
		rcvGPSData->GPSqual = atol(strtok(NULL, ","));
		rcvGPSData->satUsed = atol(strtok(NULL, ","));
		rcvGPSData->HDOP = atol(strtok(NULL, ","));
		rtnVal = 1;
	}
	else if(!strncmp(NMEABuf,"GPVTG",5))
	{
		removeSubstring(NMEABuf, ".");
		strtok(NMEABuf, ",");
		rcvGPSData->course = atol(strtok(NULL, ","));
		strtok(NULL, ","); // skip one ',' for kmph
		rcvGPSData->speed = atol(strtok(NULL, ","));
		rtnVal = 1;
	}
	
	if(rtnVal == 1 && rcvGPSData->GPSqual != 0)
	{
		pulseLed(BLUE,10);
	}
	return rtnVal;
}

void removeSubstring(char *s,const char *toRemove)
{
	while(s=strstr(s,toRemove))
	memmove(s,s+strlen(toRemove),1+strlen(s+strlen(toRemove)));
}


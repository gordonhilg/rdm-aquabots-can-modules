/*******************************************************
** File    	: cantest.c
** Author  	: Aquabots Team 2B
** Date		: 19-6-2015
** Brief	: Test functions to test the CANbus functionality of a board
**
**
*******************************************************/
#include "cantest.h"
#include "drivers/timer/timerlib.h"
#include "drivers/leds.h"

void canReceiveTest(void)
{
	//Receive
	canRcvData *receivedMessage = 0;
	
	uint16_t oldValue;
	uint16_t newValue;	
	
	while(1)
	{
		if(rcvBufferSize)
		{
			receivedMessage = getRcvBufferData();
			oldValue = newValue;
			newValue = (receivedMessage->dataBuffer[0] | receivedMessage->dataBuffer[1] << 8);
			
			if(oldValue + 1 != newValue && newValue != 0)
			{
				blinkLed(RED, 100, 5);
				blinkLed(BLUE, 100, 5);
				keepLEDOff(GREEN, 500);
			}		
				
			if(newValue == 0xFFFF)
			{
				blinkLed(BLUE, 100, 10);
			}
			
			free(receivedMessage);	
		}
	}
}

void canSendTest(void)
{
	while(1)
	{
		//Send
		uint32_t counter;
		sensorData testData;
		uint8_t sendData[2];
		testData.dataBuffer = &sendData[0];
		testData.dataSize = 2;
	
		for(counter = 0; counter != 0x10000; counter++) 
		{
			sendData[0] = counter;
			sendData[1] = counter >> 8;
			sendSensorData(&testData);
			//waitFor(2);
		}
		blinkLed(BLUE, 100, 10);
		waitFor(2500);	
	}
}